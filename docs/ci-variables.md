# CI Variables

The GitLab Cloud Native Images [CI pipelines](build.md) use variables provided by the CI environment to change build behavior between mirrors and
keep sensitive data out of the repositories.

Check the table below for more information about the various CI variables used in the pipelines.

## Build variables

| Environment Variable                          | Description |
| --------------------------------------------- | ----------- |
| GITLAB_NAMESPACE                              | GitLab group containing the rails source code repositories named by `CE_PROJECT` and `EE_PROJECT`. |
| CE_PROJECT                                    | GitLab project containing the GitLab CE source code for the gitlab-rails-ce image. |
| EE_PROJECT                                    | GitLab project containing the GitLab EE source code for the gitlab-rails-ee image. |
| FETCH_DEV_ARTIFACTS_PAT                       | Access token with permission to pull source assets from private locations. |
| ASSETS_IMAGE_REGISTRY_PREFIX                  | Pull pre-built GitLab assets container image from specified Docker registry location. |
| COMPILE_ASSETS                                | Setting `true` generates fresh rails assets instead of copying them from the assets image. Requires adjustment of `BUILDX_MEMORY` and `BUILDX_MEMORY_LIMIT` |
| AUTO_DEPLOY_COMPILE_ASSETS                    | Setting `true` causes `COMPILE_ASSETS` variable to be set to `true` in auto-deploy tag pipelines. Requires adjustment of `BUILDX_MEMORY` and `BUILDX_MEMORY_LIMIT` |
| FORCE_IMAGE_BUILDS                            | Setting `true` builds fresh images even when existing containers match the specified version. |
| FORCE_RAILS_IMAGE_BUILDS                      | Similar to `FORCE_IMAGE_BUILDS`, but only for `gitlab-rails-*` image. Setting `true` will set `FORCE_IMAGE_BUILDS` to true in `gitlab-rails-*` job definition. |
| DISABLE_DOCKER_BUILD_CACHE                    | Setting any value ensures that builds run without docker build cache. |
| UBI_PIPELINE                                  | Setting to "true" indicates this will be a UBI only pipeline. |
| CE_PIPELINE                                   | Setting any value indicates this will be a CE only pipeline. |
| EE_PIPELINE                                   | Setting any value indicates this will be an EE only pipeline. |
| CUSTOM_PIPELINE                               | Setting any value indicates this will be a custom pipeline (Do not run CE or EE specific jobs.) |
| DEPENDENCY_PROXY                              | Sets the dockerhub registry location. See [details](build.md#dependency-proxy). |
| GITLAB_BUNDLE_GEMFILE                         | Setting Gemfile path required by `gitlab-rails` bundle. If bundle uses the default Gemfile, just keep it unset. |
| ARCH_LIST                                     | Comma-separated list of architectures to build for. Default: `amd64`. Possible values: `amd64`, `arm64` |
| DISABLE_BUILDX_CLUSTER                        | Disable use of `buildx` cluster in favour of using local buildx instance. Default is `'false'` |
| DISABLE_BUILDX_ZSTD                           | Disable use of the `zstd` Zstandard fast compression algorithm that provides high compression ratios. If enabled will target default nightly images only. Default is `'true'` |
| FORCE_BUILDX_ZSTD                             | Forces the use of the `zstd` Zstandard fast compression algorithm on all images. FORCE_BUILDX_ZSTD will override DISABLE_BUILDX_ZSTD, when set to `true`. Default is `'false'` |
| BUILDKIT_IMAGE                                | Sets the image to use for running BuildKit. Default is `moby/buildkit:v0.20.1` |
| BUILDX_CLUSTER_NAME                           | Name of the GKE cluster running `buildx` agent pods. Default: `buildx` |
| BUILDX_GCLOUD_PROJECT                         | Name of the GKE project hosting `$BUILDX_CLUSTER_NAME` |
| BUILDX_CLUSTER_ZONE                           | Zone of the GKE cluster `$BUILDX_CLUSTER_NAME` |
| BUILDX_SA_JSON                                | ServiceAccount authentication credentials for `$BUILDX_CLUSTER_NAME` |
| BUILDX_REPLICAS                               | Number of `buildx` pod replicas each pipeline will instantiate. Defaults to `2` |
| BUILDX_CPU                                    | CPU resource request for `buildx` pods. Defaults to `1` |
| BUILDX_CPU_LIMIT                              | CPU resource limit for `buildx` pods. Defaults to `2` |
| BUILDX_MEMORY                                 | Memory resource request for `buildx` pods. Defaults to `2G` |
| BUILDX_MEMORY_LIMIT                           | Memory resource limit for `buildx` pods. Defaults to `5G` |
| BUILDX_AUTO_CLEANUP                           | Run `stop_buildx` at the end of pipeline without waiting for environment cleanup to take care of it. This prevents cache retention for buildx agents but allows for clean pipeline completion. | 
| TOP_UPSTREAM_SOURCE_PROJECT                   | Full path of upstream project when build pipeline was triggered from another project |
| TOP_UPSTREAM_SOURCE_REF_SLUG                  | `CI_COMMIT_SOURCE_REF_SLUG` of upstream project when build pipeline was triggered from another project |
| HIGH_CAPACITY_RUNNER_TAG                      | Custom runner tag with increased resources that can be used for more demanding builds like gitlab-rails |
| RAILS_DISABLE_VENDOR_CACHE                    | Allow disabling separate vendored gem caching for `gitlab-rails` build jobs |

## Test variables

| Environment Variable                          | Description |
| --------------------------------------------- | ----------- |
| DANGER_GITLAB_API_TOKEN                       | GitLab API token dangerbot uses to post comments on MRs. |
| NIGHTLY                                       | Set to `true` when running a nightly build. (Busts cache). |

## Release variable

| Environment Variable                          | Description |
| --------------------------------------------- | ----------- |
| GPG_KEY_AWS_ACCESS_KEY_ID                     | Account ID to read the gpg private asset signing key (for ubi assets) from a secure s3 bucket. |
| GPG_KEY_AWS_SECRET_ACCESS_KEY                 | Account secret to read the gpg private asset signing key (for ubi assets) from a secure s3 bucket. |
| GPG_KEY_LOCATION                              | Full URI location for S3 copy command for the gpg private asset signing key. |
| GPG_KEY_PASSPHRASE                            | Passphrase for using the gpg asset signing key. |
| UBI_ASSETS_AWS_ACCESS_KEY_ID                  | Account ID to read/write from the s3 bucket containing the ubi release assets. |
| UBI_ASSETS_AWS_SECRET_ACCESS_KEY              | Account secret to read/write from the s3 bucket containing the ubi release assets. |
| UBI_ASSETS_AWS_BUCKET                         | S3 bucket name for the the ubi release assets. |
| RELEASE_API                                   | Target GitLab API location when pushing release assets. |
| UBI_RELEASE_PAT                               | GitLab Private Access Token for creating a new release object on release. |
| COM_REGISTRY                                  | Docker location of the public registry. |
| COM_CNG_PROJECT                               | Project name for the public CNG project. |
| COM_REGISTRY_PASSWORD                         | Access token for syncing to the public registry. |
| REDHAT_PROJECT_JSON                           | JSON hash of OSPID and project ID. See [build details](build.md#context). |
| REDHAT_API_TOKEN                              | Red Hat token used for certification scans. |
| REDHAT_CERTIFICATION                          | Trigger Red Hat UBI certification when set to "true" |
| SCANNING_TRIGGER_PIPELINE                     | GitLab pipeline location to trigger security scanning. |
| SCANNING_TRIGGER_TOKEN                        | Trigger Token for the security scanning project. |

## Unknown/outdated variables

| Environment Variable                          | Description |
| --------------------------------------------- | ----------- |
| BUILD_TRIGGER_TOKEN                           | |
| CHART_BUILD_TOKEN                             | |
| GCR_AUTH_CONFIG                               | |
| HELM_RELEASE_BOT_PRIVATE_KEY                  | |
| FETCH_GEMS_BUNDLE_IMAGE                       | |
