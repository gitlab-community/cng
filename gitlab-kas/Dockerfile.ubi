## FINAL IMAGE ##

ARG UBI_IMAGE=
ARG UBI_MICRO_IMAGE=

FROM ${UBI_MICRO_IMAGE} AS target

FROM ${UBI_IMAGE} AS build

ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS
ARG DNF_OPTS_ROOT

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} upgrade --nodocs --best --assumeyes --setopt=install_weak_deps=0 \
  && microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 openssl-libs \
  && microdnf ${DNF_OPTS_ROOT} clean all \
  && rm -f ${DNF_INSTALL_ROOT}/var/lib/dnf/history*

ADD gitlab-kas.tar.gz ${DNF_INSTALL_ROOT}/

FROM ${UBI_MICRO_IMAGE}

ARG GITLAB_KAS_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-kas" \
      name="GitLab KAS" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_KAS_VERSION} \
      release=${GITLAB_KAS_VERSION} \
      summary="The GitLab Agent Server." \
      description="The GitLab Agent Server. Handles long-running connections from GitLab Agents for Kubernetes."

COPY --from=build ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

ENTRYPOINT ["/usr/bin/kas"]
