ARG RUBY_IMAGE=
ARG UBI_IMAGE=

FROM ${RUBY_IMAGE} AS target

FROM ${UBI_IMAGE} AS build

ARG DNF_OPTS
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS_ROOT
ARG DNF_INSTALL_ROOT=/install-root

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD gitlab-mailroom.tar.gz ${DNF_INSTALL_ROOT}/assets/

COPY scripts/ ${DNF_INSTALL_ROOT}/scripts/

RUN microdnf update -y && \
    microdnf ${DNF_OPTS} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 shadow-utils && \
    adduser -m ${GITLAB_USER} -u ${UID} -R ${DNF_INSTALL_ROOT}/ && \
    chgrp -R 0 ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} && \
    chmod -R g=u ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER}

RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 procps libicu tzdata \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && rm -f /install-root/var/lib/dnf/history*

FROM ${RUBY_IMAGE}

ARG MAILROOM_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-mailroom" \
      name="GitLab Mailroom" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${MAILROOM_VERSION} \
      release=${MAILROOM_VERSION} \
      summary="A configuration based process that will idle on IMAP connections and execute a delivery method when a new message is received." \
      description="A configuration based process that will idle on IMAP connections and execute a delivery method when a new message is received."

COPY --from=build ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

CMD ["/usr/bin/mail_room", "-c", "/var/opt/gitlab/mail_room.yml", "--log-exit-as", "json"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
