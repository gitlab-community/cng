ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG TARGETARCH
ARG AWSCLI_VERSION=1.38.7
ARG S3CMD_VERSION=2.4.0
ARG GSUTIL_VERSION=5.33
ARG PYOPENSSL_VERSION
ARG AZCOPY_STATIC_URL="https://azcopyvnext-awgzd8g7aagqhzhe.b02.azurefd.net/releases/release-10.28.0-20250127/azcopy_linux_${TARGETARCH}_10.28.0.tar.gz"

ADD gitlab-python.tar.gz /
ADD gitaly.tar.gz /tmp/gitaly

RUN mkdir /assets \
    && pip3 install pyOpenSSL==${PYOPENSSL_VERSION} \
    && pip3 install awscli==${AWSCLI_VERSION} s3cmd==${S3CMD_VERSION} gsutil==${GSUTIL_VERSION} crcmod \
    && pip3 cache purge \
    && find /usr/local/lib/python3.9 -name '__pycache__' -type d -exec rm -r {} + \
    && mkdir /tmp/azcopy \
    && curl -sL "${AZCOPY_STATIC_URL}" | tar xzf - -C /tmp/azcopy --strip-components=1 \
    && cp /tmp/azcopy/azcopy /usr/local/bin && chmod 755 /usr/local/bin/azcopy \
    && rm -rf /tmp/azcopy \
    && mv /tmp/gitaly/usr/local/bin/gitaly-backup /usr/local/bin \
    && cp -R --parents \
        /usr/local/lib/python3.9/site-packages \
        /usr/local/bin/ \
        /assets
