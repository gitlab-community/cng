ARG DEBIAN_IMAGE=debian:bookworm-slim

FROM --platform=${TARGETPLATFORM} ${DEBIAN_IMAGE} AS base

ARG RUST_VERSION=1.73.0
ARG RUST_PLATFORM=x86_64-unknown-linux-gnu
ARG BUILD_DIR=/tmp/build
ARG TARGETARCH

FROM --platform=${TARGETPLATFORM} base AS base-amd64
ARG RUST_PLATFORM=x86_64-unknown-linux-gnu
FROM --platform=${TARGETPLATFORM} base AS base-arm64
ARG RUST_PLATFORM=aarch64-unknown-linux-gnu
FROM --platform=${TARGETPLATFORM} base-$TARGETARCH
ENV RUST_URL="https://static.rust-lang.org/dist/rust-$RUST_VERSION-$RUST_PLATFORM.tar.gz"

# Install Rust
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        ca-certificates \
    && mkdir ${BUILD_DIR} \
    && cd ${BUILD_DIR} \
    && curl --retry 6 -sfo rust.tar.gz ${RUST_URL} \
    && tar -xzf rust.tar.gz \
    && rust-$RUST_VERSION-$RUST_PLATFORM/install.sh --components=rustc,cargo,rust-std-$RUST_PLATFORM --destdir=/assets \
    && rm rust.tar.gz \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf ${BUILD_DIR}
