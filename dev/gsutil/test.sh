#!/bin/bash
set -eo pipefail

errors=()

# Check for PROJECT_ID
if [ -z "$PROJECT_ID" ]; then
    errors+=("PROJECT_ID is not set.")
fi

# Check for BUCKET_URL
if [ -z "$BUCKET_URL" ]; then
    errors+=("BUCKET_URL is not set.")
elif [[ "$BUCKET_URL" != gs://* ]]; then
    errors+=("BUCKET_URL must start with 'gs://'. Current value: $BUCKET_URL")
fi

# Check for TAG
if [ -z "$TAG" ]; then
    errors+=("TAG is not set.")
fi

# If there are any errors, print them all and exit
if [ ${#errors[@]} -ne 0 ]; then
    echo "Errors:" >&2
    printf ' - %s\n' "${errors[@]}" >&2
    echo "Please set these variables before running the script." >&2
    exit 1
fi

GOOGLE_APPLICATION_CREDENTIALS="${HOME}/.config/gcloud/legacy_credentials/gcs-adc.json"
# gcloud iam service-accounts keys create \
#   ${GOOGLE_APPLICATION_CREDENTIALS} \
#   --iam-account=${IAM_ACCOUNT}

IMAGE=${IMAGE:-registry.gitlab.com/gitlab-renovate-forks/cng/gitlab-toolbox-ee}

echo "${IMAGE}:${TAG}"
echo "${GOOGLE_APPLICATION_CREDENTIALS}"

docker run --rm -t -i \
  -v "${GOOGLE_APPLICATION_CREDENTIALS}:/etc/gitlab/objectstorage/config" \
  -e "GOOGLE_APPLICATION_CREDENTIALS=/etc/gitlab/objectstorage/config" \
  -e "PROJECT_ID=${PROJECT_ID}" \
  -e "BACKUP_BUCKET_URL=${BUCKET_URL}" \
  "${IMAGE}:${TAG}" \
  bash -c '
    echo "Configuring gsutil"
    
    # Prepare
    printf "${GOOGLE_APPLICATION_CREDENTIALS}\n${PROJECT_ID}\nN\n" | gsutil config -e
    cd /tmp

    # Tests
    gsutil --version
    gsutil ls

    file_name="1234567890_1234_12_12_12_TEST_gitlab_backup.tar"
    tar -cf ${file_name} -T /dev/null

    echo "copying file ${file_name} to backup bucket ${BACKUP_BUCKET_URL}/${file_name}"
    gsutil cp ${file_name} "${BACKUP_BUCKET_URL}/${file_name}"
    gsutil ls ${BACKUP_BUCKET_URL}/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]_[0-9][0-9]_[0-9][0-9]_\*_gitlab_backup.tar
    gsutil rm "${BACKUP_BUCKET_URL}/${file_name}"
    gsutil ls ${BACKUP_BUCKET_URL}
  '